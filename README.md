# Red Bubble Test

Use `nvm use && yarn && yarn start` to get this app running.

Use `yarn test` to run tests.

If you have trouble loading localhost in a chromium browser, you may need to navigate to `chrome://net-internals/#hsts` and enter "localhost" into __Delete domain security policies__.

## Instructions

You are required to create a React app that allows a user to browse the images contained in the API. You can explore the API with this GraphQL Playground.

### Functional requirements

Your app should fetch and display all works from the following GraphQL API: https://take-home-test-gql.herokuapp.com/query Works must be displayed with the small image. The user should be able to filter by camera make and model. The app should use "Unknown Model" as the model for works that do not include a camera model. The app should use "Unknown Make" as the make for works that do not include a camera make.

### Implementation requirements

You should include thorough automated tests for your code. All of the following applies as much to your tests as to your code. You should only write minimal CSS. Focus your time on in the areas of component composition, efficient data fetching, and testing. You should set up your submission and include any necessary instructions to make it as obvious and convenient as possible for the engineer(s) reviewing your submission to install your program, run it, and run the tests. You should write clean, clear, well-factored code which you would be proud to commit at work or to an open-source project. Your code should clearly reflect and explain the problem domain. You should write code which you can easily extend. We'll ask you to do so later in your interview.

### Submission guidelines

All of the affirmative requirements above (the ones that say "should") are important. Be sure to address them all and not miss any. If you don't understand any of them, just ask. We're not timing you, and you can take as much time as you like, but this test is intended to take you no more than a few hours.

If you use version control while developing your submission, please include the version history with your submission. This allows us to see the process that you followed while you worked. For example, if you commit to git as you worked, please include the .git directory in the zip file that you send us.

We'll evaluate you most fairly if only our talent team knows whose submission is being evaluated. Please don't put your name or other identifying information in your submission. If you use version control while developing your submission and include the version history with your submission, please be sure that your version control system doesn't record your identity.

For example, if you use git, mask your name and email before you commit anything with: git config user.name "Redbubble Applicant" git config user.email me@example.com And do send us a zip file, not just a link to a repository online which would give away your identity.
