import React, { StrictMode } from "react";
import ReactDOM from "react-dom";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import { ThemeProvider } from "styled-components";
import { FontFace } from "./styles/settings/font-face";
import GlobalStyle from "./styles/global-style";
import theme from "./styles/theme";
import Gallery from "./components/gallery/gallery";

const client = new ApolloClient({
    uri: "https://take-home-test-gql.herokuapp.com/query",
    cache: new InMemoryCache()
});

const App = () => (
    <StrictMode>
      <ApolloProvider client={client}>
        <ThemeProvider theme={theme}>
          <FontFace />
          <GlobalStyle />
          <Gallery />
        </ThemeProvider>
      </ApolloProvider>
    </StrictMode>
);

ReactDOM.render(<App />, document.getElementById("root"));