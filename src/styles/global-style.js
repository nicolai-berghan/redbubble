import { createGlobalStyle } from "styled-components";
import { reset } from "styled-reset";
import { normalize } from "styled-normalize";

const GlobalStyle = createGlobalStyle`
	${reset}
	${normalize}

	html {
		box-sizing: border-box;
    	overflow-x: hidden;
	}
	*, *:before, *:after {
		box-sizing: inherit;
	}

	body {
		margin: 0;
		font-family: ${({ theme }) => theme.font.family.primary};
		padding: ${({ theme }) => theme.spacing.medium}
	}

	p {
		margin-bottom: ${({ theme }) => theme.spacing.small}
	}
`;

export default GlobalStyle;
