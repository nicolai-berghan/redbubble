const theme = {
	font: {
		family: {
			primary: '"Karla", sans-serif',
		},
	},
	spacing: {
		small: "8px",
		medium: "16px",
		large: "24px",
		xlarge: "48px",
	},
	color: {
		black: "#000000",
		white: "#ffffff",
	},
};
export default theme;
