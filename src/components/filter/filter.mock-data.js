const mockData = {
    "data": {
        "works": [{
                "exif": {
                    "model": "NIKON D80",
                    "make": "NIKON CORPORATION"
                }
            },
            {
                "exif": {
                    "model": "Canon EOS 20D",
                    "make": "Canon"
                }
            },
            {
                "exif": {
                    "model": "FinePix S6500fd",
                    "make": "FUJIFILM"
                }
            },
            {
                "exif": {
                    "model": "",
                    "make": ""
                }
            },
            {
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "exif": {
                    "model": "SLP1000SE",
                    "make": "FUJI PHOTO FILM CO., LTD."
                }
            },
            {
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "exif": {
                    "model": "DMC-FZ30",
                    "make": "Panasonic"
                }
            },
            {
                "exif": {
                    "model": "DMC-FZ30",
                    "make": "Panasonic"
                }
            },
            {
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "exif": {
                    "model": "Canon EOS 400D DIGITAL",
                    "make": "Canon"
                }
            },
            {
                "exif": {
                    "model": "",
                    "make": ""
                }
            }
        ]
    }
}

export default mockData;