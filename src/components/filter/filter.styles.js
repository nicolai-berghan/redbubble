import styled from "styled-components";

export const Filter = styled.div`
	display: flex;
	gap: ${({ theme }) => theme.spacing.medium};
	margin-bottom: ${({ theme }) => theme.spacing.medium};
`;

export const Select = styled.div`
	label {
		display: block;
		margin-bottom: ${({ theme }) => theme.spacing.small};
	}
`;
