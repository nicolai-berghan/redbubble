import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useQuery } from "@apollo/client";
import * as s from "./filter.styles";
import { GET_FILTER_PARAMS } from "./filter.graphql";

const Filter = ({ setMake, setModel }) => {
    const { loading, error, data } = useQuery(GET_FILTER_PARAMS);
    const [makes, setMakes] = useState(null);
    const [models, setModels] = useState(null);

    function setFilterItems(data) {
        const makes = new Set();
        const models = new Set();
        data.works.forEach(work => {
            const { make, model } = work.exif;
            makes.add(make.length ? make : "Unknown Make");
            models.add(model.length ? model : "Unknown Model");
        });
        setMakes(makes);
        setModels(models);
    }

    useEffect(() => {
        if (data) setFilterItems(data);
    }, [data])

    if (loading) return <p>Loading...</p>;
    if (error) {
        console.warn(error);
        return <p>Error :(</p>;
    }

    return (
        <s.Filter data-test="filter">
			<s.Select>
				<label htmlFor="make">Filter by Make</label>
				<select 
					name="make"
					id="make" 
					onBlur={e => setMake(e.target.value)}
					onChange={e => setMake(e.target.value)}
					data-test="filter-make"
				>	
					<option value="all">Show All Makes</option>
                    {makes && Object.values([...makes]).map(make => (
                        <option key={make} value={make}>{make}</option>
                    ))}
				</select>
			</s.Select>
			<s.Select>
				<label htmlFor="model">Filter by Model</label>
				<select
					name="model"
					id="model"
					onBlur={e => setModel(e.target.value)}
					onChange={e => setModel(e.target.value)}
					data-test="filter-model"
				>
					<option value="all">Show All Models</option>
					{models && Object.values([...models]).map(model => (
						<option key={model} value={model}>{model}</option>
					))}
				</select>
			</s.Select>
		</s.Filter>
    );
};

Filter.propTypes = {
    setMake: PropTypes.func.isRequired,
    setModel: PropTypes.func.isRequired
};

export default Filter;