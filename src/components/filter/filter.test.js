import Filter from "./filter";
import { GET_FILTER_PARAMS } from "./filter.graphql";
import { mount } from "enzyme";
import toJSON from "enzyme-to-json";
import wait from "waait";
import { MockedProvider } from '@apollo/client/testing';
import mockData from "./filter.mock-data"
import { act } from "react-dom/test-utils";
import { ThemeProvider } from "styled-components";
import theme from "../../styles/theme";

describe("<Filter/>", () => {
    const setParam = jest.fn();
    const mocks = [{
        request: { query: GET_FILTER_PARAMS },
        result: mockData
    }];
    const wrapper = mount(
        <MockedProvider mocks={mocks}>
            <ThemeProvider theme={theme}>
                <Filter setMake={setParam} setModel={setParam} />
            </ThemeProvider>
        </MockedProvider>
    );

    it("renders with data", async () => {
        expect(wrapper.text()).toContain("Loading...");
        await act(() => wait());
        wrapper.update();
        expect(toJSON(wrapper.find("[data-test='filter']"))).toMatchSnapshot();
    });
})