import { gql } from "@apollo/client";

const GET_FILTER_PARAMS = gql `
  query geFilterParams {
    works {
      exif {
        make
        model
      }
    }
  }
`;

export { GET_FILTER_PARAMS };