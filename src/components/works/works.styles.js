import styled from "styled-components";

export const Works = styled.div`
	display: flex;
	flex-wrap: wrap;
	gap: ${({ theme }) => theme.spacing.medium};
`;
