import React from "react";
import PropTypes from "prop-types";
import { useQuery } from "@apollo/client";
import * as s from "./works.styles";
import { GET_WORKS } from "./works.graphql";

const Works = ({ filterParams }) => {
    const { make, model } = filterParams;
    const { loading, error, data } = useQuery(GET_WORKS);

    const matches = (exifParam, filterParam) => {
        if (filterParam === "Unknown Make" && !exifParam.length) return true;
        if (filterParam === "Unknown Model" && !exifParam.length) return true;
        return filterParam === "all" || filterParam === exifParam;
    };

    if (loading) return <p>Loading...</p>;
    if (error) {
        console.warn(error);
        return <p>Error :(</p>;
    }

    return (
        <s.Works data-test="works">
            {data.works.map(work => {
              if (matches(work.exif.make, make) && matches(work.exif.model, model))
                return (
                  <img
                    key={work.id}
                    src={work.urls.filter(url => url.type === "small")[0].link}
                    alt={work.filename}
                  />
                );
              return null;
            })}
      </s.Works>
    );
};

Works.propTypes = {
    filterParams: PropTypes.shape({
        make: PropTypes.string,
        model: PropTypes.string
    }).isRequired
};

export default Works;