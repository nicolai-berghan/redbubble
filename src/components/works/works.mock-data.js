const mockData = {
    "data": {
        "works": [{
                "id": "31820",
                "filename": "162042.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.31820.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.31820.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.31820.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "NIKON D80",
                    "make": "NIKON CORPORATION"
                }
            },
            {
                "id": "2041",
                "filename": "2010.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.2041.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.2041.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.2041.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "Canon EOS 20D",
                    "make": "Canon"
                }
            },
            {
                "id": "240509",
                "filename": "261748.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.240509.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.240509.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.240509.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "FinePix S6500fd",
                    "make": "FUJIFILM"
                }
            },
            {
                "id": "26583",
                "filename": "27112.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.26583.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.26583.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.26583.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "",
                    "make": ""
                }
            },
            {
                "id": "2729606",
                "filename": "2849897.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.2729606.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.2729606.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.2729606.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "id": "2828069",
                "filename": "2953043.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.2828069.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.2828069.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.2828069.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "SLP1000SE",
                    "make": "FUJI PHOTO FILM CO., LTD."
                }
            },
            {
                "id": "3201430",
                "filename": "3348825.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.3201430.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.3201430.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.3201430.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "id": "3502552",
                "filename": "3664692.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.3502552.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.3502552.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.3502552.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "id": "3745978",
                "filename": "3919261.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.3745978.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.3745978.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.3745978.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "DMC-FZ30",
                    "make": "Panasonic"
                }
            },
            {
                "id": "3775226",
                "filename": "3949523.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.3775226.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.3775226.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.3775226.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "DMC-FZ30",
                    "make": "Panasonic"
                }
            },
            {
                "id": "512919",
                "filename": "550863.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.512919.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.512919.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.512919.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "id": "4250369",
                "filename": "6783468.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.4250369.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.4250369.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.4250369.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "D-LUX 3",
                    "make": "LEICA"
                }
            },
            {
                "id": "777577",
                "filename": "827375.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.777577.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.777577.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.777577.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "Canon EOS 400D DIGITAL",
                    "make": "Canon"
                }
            },
            {
                "id": "867035",
                "filename": "920668.jpg",
                "urls": [{
                        "type": "small",
                        "link": "http://ih1.redbubble.net/work.867035.1.flat,135x135,075,f.jpg"
                    },
                    {
                        "type": "medium",
                        "link": "http://ih1.redbubble.net/work.867035.1.flat,300x300,075,f.jpg"
                    },
                    {
                        "type": "large",
                        "link": "http://ih1.redbubble.net/work.867035.1.flat,550x550,075,f.jpg"
                    }
                ],
                "exif": {
                    "model": "",
                    "make": ""
                }
            }
        ]
    }
}

export default mockData;