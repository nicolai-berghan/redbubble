import Works from "./works";
import { GET_WORKS } from "./works.graphql";
import { mount } from "enzyme";
import toJSON from "enzyme-to-json";
import wait from "waait";
import { MockedProvider } from '@apollo/client/testing';
import mockData from "./works.mock-data"
import { act } from "react-dom/test-utils";
import { ThemeProvider } from "styled-components";
import theme from "../../styles/theme";

describe("<Works/>", () => {
    const mocks = [{
        request: { query: GET_WORKS },
        result: mockData
    }];
    const filterParams = {
        make: "NIKON D80",
        model: "NIKON CORPORATION",
    }
    const wrapper = mount(
        <MockedProvider mocks={mocks}>
            <ThemeProvider theme={theme}>
                <Works filterParams={filterParams}/>
            </ThemeProvider>
        </MockedProvider>
    );

    it("renders with data", async () => {
        expect(wrapper.text()).toContain("Loading...");
        await act(() => wait());
        wrapper.update();
        expect(toJSON(wrapper.find("[data-test='works']"))).toMatchSnapshot();
    });
})