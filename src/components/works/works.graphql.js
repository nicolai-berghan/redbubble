import { gql } from "@apollo/client";

const GET_WORKS = gql`
  query getWorks {
    works {
      id
      filename
      urls {
        type
        link
      }
      exif {
        make
        model
      }
    }
  }
`;

export { GET_WORKS };
