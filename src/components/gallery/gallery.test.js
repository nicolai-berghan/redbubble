import Gallery from "./gallery"
import { shallow } from 'enzyme';
import toJSON from "enzyme-to-json";

describe("<Gallery/>", () => {
    const wrapper = shallow(<Gallery/>);

    it("renders the gallery correctly", () => {
        expect(toJSON(wrapper)).toMatchSnapshot();
    });
})