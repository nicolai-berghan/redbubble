import React, { useState } from "react";
import * as s from "./gallery.styles";
import Filter from "../filter/filter";
import Works from "../works/works";

const Gallery = () => {
    const [make, setMake] = useState("all");
    const [model, setModel] = useState("all");

    return (
        <s.Gallery>
          <Filter setMake={setMake} setModel={setModel} />
          <Works filterParams={{ make, model }} />
        </s.Gallery>
    );
};

export default Gallery;